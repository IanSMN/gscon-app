import { combineReducers } from "redux";
import HubReducer from "../reducers/HubReducer";

export default combineReducers({
    HubReducer
});
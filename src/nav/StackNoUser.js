import { createStackNavigator, createAppContainer } from "react-navigation";
import Simulacao from "../Views/Simulacao/Simulacao";
import Planos from "../Views/Planos/Planos";

const StackNoUser = createStackNavigator(
    {
        Simulacao: {
            screen: Simulacao
        },
        Planos: {
            screen: Planos
        }
    },
    {
        initialRouteName: 'Simulacao',
        headerMode: 'none'
    }
)

export default createAppContainer(StackNoUser)
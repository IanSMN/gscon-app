import { createStackNavigator, createAppContainer } from "react-navigation";
import Login from '../Views/Login/Login';
import Slides from "../Views/Slides/Slides";
import Home from "../Views/Home/Home";

const StackAuth = createStackNavigator(
    {
        Login: {
            screen: Login
        },
        Slides: {
            screen: Slides
        },
        Home: {
            screen: Home
        }
    },
    {
        initialRouteName: 'Slides',
        headerMode: 'none'
    }
)

export default createAppContainer(StackAuth)
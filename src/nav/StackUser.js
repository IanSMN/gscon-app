import React from 'react';
import { View } from 'react-native';
import { createBottomTabNavigator, createAppContainer } from "react-navigation";
import UserHome from "../Views/UserHome/UserHome";
import { red, blue } from '../utils/Colors';
import { Icon } from 'native-base';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Assembleia from '../Views/Assembleia/Assembleia';

class TabIcon extends React.Component {
    render() {
        const { name, color, size } = this.props;
        return (
            <View style={{ width: 24, height: 24, margin: 5 }}>
                <Icon
                    style={{ fontSize: size }}
                    type='FontAwesome'
                    color={color}
                    name={name}
                />
            </View>
        )
    }
}

// const IconFunc = props => {
//     return <TabIcon {...props} />
// }

const getTabBarIcon = (navigation, focused, tintColor) => {
    const { routeName } = navigation.state;
    let IconComponent = FontAwesome;
    let iconName;
    if (routeName === 'Home') {
        iconName = 'home';
        return (
            // <TabIcon size={24} color={tintColor} name={iconName} />
            <IconComponent name={iconName} size={25} color={tintColor} />
        )
    } else {
        iconName = 'users';
        // IconComponent = IconFunc;
        return (
            <IconComponent name={iconName} size={25} color={tintColor} />
            // <TabIcon size={24} color={tintColor} name={iconName} />
        )
    }
}

const StackUser = createBottomTabNavigator({
    Assembléia: {
        screen: Assembleia
    },
    Contemplação: {
        screen: UserHome
    },
    Home: {
        screen: UserHome
    },
    Boletos: {
        screen: UserHome
    },
    Simulação: {
        screen: UserHome
    },
},
    {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, tintColor }) =>
            getTabBarIcon(navigation, focused, tintColor),
        }),
        tabBarOptions: {
            activeTintColor: red,
            inactiveTintColor: blue,
        },
        initialRouteName: 'Home',
    },
)

export default createAppContainer(StackUser)
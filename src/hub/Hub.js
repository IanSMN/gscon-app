import React from 'react';
import { Text } from 'react-native'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { handleSplash } from '../actions/hubActions'
import Splash from '../components/Splash/Splash';
import StackAuth from '../nav/StackAuth';

class Hub extends React.Component {

    componentDidMount() {
        setTimeout(() => { this.props.handleSplash() }, 2500)
    }

    render() {
        if (this.props.splashOn) {
            return <Splash />
        } else {
            return <StackAuth />
        }
    }
}

const mapStateToProps = state => ({
    splashOn: state.HubReducer.splashOn
})

const mapDispatchToProps = dispatch => bindActionCreators({ handleSplash }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Hub)

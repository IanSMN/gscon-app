const INITIAL_STATE = {
    valorBem: null,
    valorParcelas: null,
    valorTotal: null,
    numeroParcelas: null
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'HANDLE_SIMULACAO':
            return {
                ...state,
                valorBem: action.payload.valorBem,
                valorParcelas: action.payload.valorParcela,
                valorTotal: action.payload.valorBem * 1.1,
                numeroParcelas: action.payload.prazo
            }
    }
}
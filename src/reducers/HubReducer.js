const INITIAL_STATE = {
    splashOn: true
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'SPLASH':
            return { ...state, splashOn: false };
        default:
            return state;
    }
};

import React from 'react'
import { StyleSheet } from 'react-native'
import { Header, Left, Button, Icon, Title, Body, Right } from 'native-base';
import { blue, clear } from '../../utils/Colors';

const Toolbar = (props) => (

    <Header
        androidStatusBarColor={blue}
        style={estilo.header}>

        <Left>
            {props.leftIcon ?
                <Button transparent onPress={props.leftAction}>
                    <Icon
                        type='FontAwesome'
                        name={props.leftIcon}
                        style={estilo.icone}
                    />
                </Button>
                : null
            }
        </Left>
        <Body>
            {props.titleIcon ?
                <Icon
                    type='FontAwesome'
                    name={props.titleIcon}
                    style={estilo.iconeTitulo}
                />
                : null
            }
            <Title style={{ fontSize: 22, color: clear }}>
                {props.titulo}
            </Title>
        </Body>
        <Right>
            {props.rightIcon ?
                <Button transparent onPress={props.rightIcon}>
                    <Icon
                        type='FontAwesome'
                        name={props.rightIcon}
                        style={estilo.icone}
                    />
                </Button>
                : null
            }
        </Right>
    </Header>
)

const estilo = StyleSheet.create({
    header: { elevation: 4, zIndex: 10 },
    icone: { fontSize: 28, color: clear },
    iconeTitulo: { fontSize: 36, color: clear },
    // contentCenter: {
    //     justifyContent: 'center',
    //     alignItems: 'center',
    // }
})

export default Toolbar


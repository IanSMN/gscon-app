// import React from 'react'
// import { Text, View, TouchableOpacity, Modal, StyleSheet } from 'react-native'
// import { second_info } from '../../utils/Colors';
// // import console = require('console'); 

// class Dropdown extends React.Component {
//     constructor(props) {
//         super(props)
//         this.state = {
//             selecionado: null
//         }
//     }

//     _selectOption(opcao) {
//         this.setState({ selecionado: opcao })
//         this.props.selecionar()
//     }

//     render() {
//         return (
//             <Modal
//                 animationType="slide"
//                 transparent={true}
//                 visible={this.props.visivel}
//                 onRequestClose={this.props.fechar}
//             >
//                 <View style={estilo.wrapper}>
//                     <View style={estilo.content}>
//                         <View style={estilo.header}>
//                             <Text style={{ fontSize: 20, fontWeight: 'bold' }}>
//                                 Selecione o prazo:
//                         </Text>
//                         </View>
//                         {this.props.opcoes.map((opcao, i) => (
//                             <TouchableOpacity
//                                 style={estilo.opcao}
//                                 key={i}
//                                 onPress={() => this._selectOption(opcao)}
//                             >
//                                 <Text style={{ fontSize: 20 }}>
//                                     {opcao.label}
//                                 </Text>
//                             </TouchableOpacity>
//                         ))}
//                     </View>
//                 </View>
//             </Modal>
//         )
//     }
// }

// const estilo = StyleSheet.create({
//     wrapper: {
//         position: 'absolute',
//         width: '100%',
//         height: '100%',
//         top: 0,
//         left: 0,
//         backgroundColor: 'rgba(0, 0, 0, 0.6)',
//         justifyContent: 'center',
//         alignItems: 'center',
//         zIndex: 9
//     },
//     content: {
//         width: '98%',
//         backgroundColor: '#fff',
//         padding: 10,
//         borderRadius: 4
//     },
//     opcao: {
//         width: '100%',
//         justifyContent: 'center',
//         alignItems: 'center',
//         height: 50,
//         borderBottomWidth: 1,
//         borderBottomColor: second_info
//     },
//     header: {
//         width: '100%',
//         borderBottomWidth: 1,
//         borderBottomColor: second_info,
//         height: 50,
//         paddingRight: 10,
//         justifyContent: 'flex-start',
//         alignItems: 'center',
//         backgroundColor: '#fff'
//     }
// })

// export default Dropdown
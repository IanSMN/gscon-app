import React from 'react'
import { View, StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback } from 'react-native'
import { dark_gray } from '../../utils/Colors';
import { Icon } from 'native-base';

const InputDropdown = (props) => (
    <TouchableWithoutFeedback onPress={props.clique}>
        <View
            style={estilo.container}>
            <Text style={estilo.placeholder}>{props.placeholder}</Text>
            <TouchableOpacity
            onPress={props.clique}
            style={estilo.dropwButton}>
                <Icon
                    type='FontAwesome'
                    name='chevron-down'
                    style={{ fontSize: 24, color: dark_gray }}
                />
            </TouchableOpacity>
        </View>
    </TouchableWithoutFeedback>
)

const estilo = StyleSheet.create({
    container: {
        width: '100%',
        height: 50,
        paddingLeft: 4,
        backgroundColor: '#fff',
        borderRadius: 4,
        borderWidth: 1,
        borderColor: dark_gray,
        paddingLeft: 10,
        paddingRight: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    placeholder: {
        color: dark_gray,
        fontSize: 18
    }
})

export default InputDropdown
import React from 'react';
import {
    Text, View, TextInput, TouchableOpacity, Animated
} from 'react-native';
import { InputStyle } from './InputStyle';
import { dark_gray, second_info, blue } from '../../utils/Colors';
import { Icon } from 'native-base';

export default class Input extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inputFocus: false,
            inputValidade: true,
            inputDirty: false,
            securityText: this.props.type === 'password' ? true : false,
            labelPosition: this.props.value === '' ? new Animated.Value(15) : -5
        }
    }

    _handleChange = (value) => {
        this.props.onChange(this.props.name, value)
        this.setState({ inputDirty: true })
    }

    _handleTouch = () => {
        this.props.onTouch(this.props.name)
    }

    _focus = () => {
        this.setState({ ...this.state, inputFocus: true })
        if (this.props.value === '') {
            this._animationLabelFocus()
        } else {
            null
        }
    }

    // _blur = () => {
    //     this.setState({ ...this.state, inputFocus: false })
    // }

    _animationLabelFocus = () => {
        Animated.timing(this.state.labelPosition, {
            toValue: -25,
            duration: 200
        }).start()
    }

    render() {
        const {
            label,
            error,
            value,
            placeholder,
            onSubmitEditing,
            refInput,
            type,
            masked,
            options,
            sendingInput,
            action, actionIcon,
            inputAction,
            required,
            dropdown,
            ...rest
        } = this.props

        return (
            <View style={InputStyle.groupInput}>
                <Animated.Text style={[
                    InputStyle.label,
                    this.state.inputFocus || (!this.state.inputFocus && value !== '') || (value !== '') ?
                        [InputStyle.labelFocus, { top: this.state.labelPosition }] :
                        [InputStyle.labelNormal, { top: 35 }],
                    error ? [InputStyle.labelError, { top: this.state.labelPosition }] : null]}>
                    {label} {required ? '*' : null}
                </Animated.Text>

                <TextInput
                    style={[InputStyle.textInput,
                    error ? InputStyle.inputError :
                        (this.state.inputFocus ? InputStyle.inputFocus : InputStyle.input)]}
                    underlineColorAndroid='transparent'
                    placeholder={this.state.inputFocus ?
                        placeholder : null}
                    placeholderTextColor={second_info}
                    onChangeText={this._handleChange}
                    onBlur={this._handleTouch}
                    onFocus={() => this._focus()}
                    onSubmitEditing={onSubmitEditing}
                    secureTextEntry={this.state.securityText}
                    ref={refInput}
                    value={value}
                    autoCapitalize={'none'}
                    {...rest}
                />

                {error &&
                    <View style={{ width: '100%' }}>
                        <Text style={{
                            color: 'red',
                            fontSize: 16,
                            position: 'absolute'
                        }}>{error}</Text>
                    </View>
                }

                {/* {dropdown &&
                    <TouchableOpacity
                        style={{ position: 'absolute', right: 5, top: 4 }}
                        onPress={() => console.warm('ABRE O DROPDOWN')}>
                        <Icon
                            type='FontAwesome'
                            name="chevron-circle-down"
                            style={{ fontSize: 24, color: blue }}
                        />
                    </TouchableOpacity>
                } */}

            </View>
        )

    }
}
import { StyleSheet } from 'react-native';
import { dark_gray, red, blue } from '../../utils/Colors';

export const InputStyle = StyleSheet.create({
    groupInput: {
        width: '100%',
        position: 'relative',
        height: 80
    },

    label: {
        zIndex: 2,
        marginLeft: 4,
    },

    labelNormal: { color: dark_gray, fontSize: 17 },

    labelFocus: {
        fontSize: 15,
        color: blue,
        position: 'absolute',
        // top: -5
    },
    labelError: {
        fontSize: 15,
        color: red,
        position: 'absolute',
        top: -5
    },
    textInput: {
        width: '100%',
        height: 50,
        fontSize: 22,
        color: dark_gray,
        paddingLeft: 4,
        backgroundColor: '#fff',
        borderRadius: 4,
        borderWidth: 1
    },
    input: {
        borderColor: dark_gray
    },
    inputError: {
        borderColor: red
    },
    inputFocus: {
        borderColor: blue
    }
})
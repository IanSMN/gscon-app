import React from 'react'
import { TouchableOpacity, Text } from 'react-native'
import { second_info, clear } from '../../utils/Colors';
import { Icon } from 'native-base';

const Button = (props) => (

    <TouchableOpacity
        style={{
            width: props.width,
            height: props.height ? props.height : 48,
            borderRadius: props.borderRadius ? props.borderRadius : 4,
            backgroundColor: props.disabled ? second_info : (props.borda ? 'transparent' : props.corFundo),
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'row',
            borderColor: props.borda ? props.corBorda : 'transparent',
            borderWidth: props.borda ? 1 : 0
        }}
        onPress={props.onPress}
        disabled={props.disabled}
    >

        {
            props.leftIcon ?

                <Icon
                    type={'FontAwesome'}
                    name={props.leftIcon}
                    style={{ fontSize: 17, color: clear, marginRight: 10 }}
                />
                : null
        }

        <Text style={{
            color: props.borda ? props.corBorda : props.corTexto,
            fontSize: 16
        }}>{props.texto}</Text>

        {
            props.rightIcon ?

                <Icon
                    type={'FontAwesome'}
                    name={props.rightIcon}
                    style={{ fontSize: 17, color: clear, marginLeft: 10 }}
                />
                : null
        }

    </TouchableOpacity>
)

export default Button
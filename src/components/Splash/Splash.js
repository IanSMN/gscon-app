import React from 'react';
import { Image, StyleSheet, View, Dimensions } from 'react-native';

const Splash = () => (
    <View style={styles.container}>
        <View style={styles.img_container}>
            <Image
                source={require('../../assets/img/groscon-logo.png')}
                resizeMode='contain'
                style={{ width: '80%' }}
            />
        </View>

        <Image
            style={styles.corner_img}
            source={require('../../assets/img/bcb.png')}
            resizeMode={'contain'}
        />

    </View>
)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    img_container: {
        width: Dimensions.get('window').width,
        justifyContent: 'center',
        alignItems: 'center'
    },
    corner_img: {
        position: 'absolute',
        bottom: 10,
        right: 10,
        width: 150,
    }
})

export default Splash
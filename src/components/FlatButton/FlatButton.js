import React from 'react'
import { TouchableOpacity, Text } from 'react-native'

const FlatButton = (props) => (
    <TouchableOpacity
        style={{
            width: props.width,
            height: 48,
            backgroundColor: 'transparent',
            justifyContent: 'center',
            alignItems: 'center',
        }}
        onPress={props.onPress}
        disabled={props.disabled}
    >

        <Text style={{
            color: props.corTexto,
            fontSize: 16,
        }}>{props.texto}
        </Text>
    </TouchableOpacity>
)

export default FlatButton
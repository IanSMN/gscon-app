import React from 'react'
import { compose, createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import Hub from '../hub/Hub';
import Reducer from '../utils/Reducer';

const composeEnhancers = compose

const Main = () => (
    <Provider store={createStore(Reducer, composeEnhancers(applyMiddleware(thunk)))}>
        <Hub />
    </Provider>
)

export default Main
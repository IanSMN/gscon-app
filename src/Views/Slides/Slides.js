import React from 'react'
import { View, Text, StyleSheet, ImageBackground } from 'react-native'
import Swiper from 'react-native-swiper'
import { blue, clear } from '../../utils/Colors';
import Button from '../../components/Button/Button';

const Slides = (props) => (
    <View style={estilo.container}>
        <View style={estilo.slideContainer}>
            <Swiper autoplay={true}>

                <ImageBackground
                    style={estilo.imgBackground}
                    source={require('../../assets/img/compra-casa.jpg')}
                    resizeMode='cover'
                >
                    <Text style={estilo.title}>Imóveis</Text>
                    <Text style={estilo.description}>
                        Compre sua casa própria, reforme seu imóvel
                            ou amplie seu patrimônio.
                    </Text>
                </ImageBackground>

                <ImageBackground style={estilo.imgBackground}
                    source={require('../../assets/img/compra-carro.jpg')}
                    resizeMode='cover'>
                    <Text style={estilo.title}>Imóveis</Text>
                    <Text style={estilo.description}>
                        Compre sua casa própria, reforme seu imóvel
                            ou amplie seu patrimônio.
                    </Text>
                </ImageBackground>

                <ImageBackground style={estilo.imgBackground}
                    source={require('../../assets/img/compra-moto.jpg')}
                    resizeMode='cover'>
                    <Text style={estilo.title}>Imóveis</Text>
                    <Text style={estilo.description}>
                        Compre sua casa própria, reforme seu imóvel
                            ou amplie seu patrimônio.
                    </Text>
                </ImageBackground>

            </Swiper>
        </View>

        <View style={estilo.buttonContainer}>
            <View style={estilo.buttonView}>
                <Button
                    corFundo={blue}
                    texto="É CLIENTE? FAÇA SEU LOGIN AQUI"
                    width='85%'
                    corTexto={clear}
                    onPress={() => props.navigation.navigate('Login')}
                />
            </View>
            <View style={[estilo.buttonView, { marginTop: 20 }]}>
                <Button
                    borda={true}
                    corBorda={blue}
                    texto="CONTINUAR SEM LOGIN"
                    width='85%'
                    onPress={() => props.navigation.navigate('Home', { usuario: false })}
                />
            </View>
        </View>

    </View>
)

export default Slides

const estilo = StyleSheet.create({
    container: {
        flex: 4
    },
    slideContainer: {
        flex: 2.5,
    },
    buttonContainer: {
        flex: 1.5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonView: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    imgBackground: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 24,
        color: clear,
        textAlign: 'center'
    },
    description: {
        fontSize: 17,
        color: clear,
        textAlign: 'center'
    }
})
import React from 'react'
import { Text, StyleSheet, View, Image } from 'react-native';
import { Container, Content } from 'native-base';
import Toolbar from '../../components/Toolbar/toolbar';
import { second_info, blue, dark_gray, green } from '../../utils/Colors';
import Button from '../../components/Button/Button';

export default (props) => (
    <Container>
        <Toolbar
            titulo="Assembléia"
            leftIcon="arrow-left"
            leftAction={() => props.navigation.navigate('Home')}
        />
        <Content contentContainerStyle={estilo.wrapper}>
            <View style={[estilo.border, estilo.w100, estilo.center_around,
            estilo.row, estilo.p6, { flex: 1 }]}
            >
                <Image
                    style={{ width: 104, height: 55 }}
                    source={require('../../assets/img/carro.png')}
                />

                <View style={{ flexDirection: 'column' }}>
                    <Text style={[estilo.bold, { fontSize: 16 }]}>Grupo 4458   Cota nº 85</Text>
                    <Text>Valor Cota: R$ 34.210</Text>
                    <Text>Automóvel Fiat Mobi 1.0 Evo Easy </Text>
                </View>

            </View>

            <View style={[estilo.border, estilo.w100, estilo.p6, { flex: 1.6 }]}>

                <View style={[estilo.column, estilo.w100, { textAlign: 'center' }]}>
                    <Text style={estilo.titleType}>
                        Crédito Disponível
                    </Text>
                    <Text style={[estilo.bold, { color: green, fontSize: 17 }]}>
                        R$ 27.621,00
                    </Text>
                </View>
            </View>

            <View style={[estilo.border, estilo.w100, { flex: 2.2 }]}>
                <Text>Slide ano</Text>
            </View>

            <View style={[estilo.w100, { flex: 1.4, padding: 8 }]}>
                <View style={[estilo.card, estilo.w100, estilo.row,
                estilo.center_around, estilo.p6]}>
                    <Image
                        style={{ width: 50, height: 50 }}
                        source={require('../../assets/img/lance.png')}
                    />
                    <View style={{ textAlign: 'center' }}>
                        <Text style={[estilo.blue, { fontSize: 16 }]}>OFERTA DE LANCE</Text>
                        <Text style={estilo.bold}>Você já fez o seu lance esse mês?</Text>
                    </View>
                </View>
                <Button
                    width={'100%'}
                    height={35}
                    texto='DÊ SEU LANCE AGORA!'
                    corTexto='#fff'
                    corFundo={blue}
                    borderRadius={6}
                />
            </View>
        </Content>
    </Container>
)

const estilo = StyleSheet.create({
    wrapper: {
        flex: 6,
        justifyContent: 'center',
        alignItems: 'center'
    },
    border: { borderBottomColor: second_info, borderBottomWidth: 1 },
    w100: { width: '100%' },
    center_around: {
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    card: {
        backgroundColor: '#fff',
        borderColor: second_info,
        borderWidth: 1,
        borderRadius: 4,
        marginBottom: 3
    },
    row: { flexDirection: 'row' },
    column: { flexDirection: 'column' },
    p6: { padding: 6 },
    blue: { color: blue },
    bold: { fontWeight: 'bold' },
    titleType: { fontSize: 19, color: dark_gray, fontWeight: 'bold' }
})
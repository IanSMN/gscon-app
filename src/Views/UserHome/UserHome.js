import React from 'react'
import { View, StyleSheet, Text, Image, TouchableOpacity, ImageBackground, ScrollView } from 'react-native'
import { blue, bg_gray, second_info } from '../../utils/Colors';
import { Icon } from 'native-base';
import Swiper from 'react-native-swiper';
import Button from '../../components/Button/Button';

export default class UserHome extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <ScrollView scrollEnabled={true} contentContainerStyle={[estilo.container]}>
                <View style={[estilo.topBar, estilo.w100, estilo.row, estilo.center_around]}>
                    <View style={[estilo.column, { width: '22%', alignItems: 'center' }]}>
                        <View style={[estilo.row, estilo.between_center, estilo.w100]}>
                            <Text style={estilo.blueText}>GRUPO</Text>
                            <Text>4458</Text>
                        </View>
                        <View style={[estilo.row, estilo.between_center, estilo.w100]}>
                            <Text style={estilo.blueText}>COTA</Text>
                            <Text>85</Text>
                        </View>
                    </View>

                    <Image
                        source={require('../../assets/img/groscon-logo.png')}
                        resizeMode='contain'
                        style={{ width: '40%' }}
                    />

                    <View style={[estilo.row, estilo.between_center, { width: '25%' }]}>
                        <TouchableOpacity style={[estilo.center_center, estilo.size40]}>
                            <Icon
                                type='FontAwesome'
                                style={estilo.tabIcon}
                                name="bell"
                            />
                        </TouchableOpacity>
                        <TouchableOpacity style={[estilo.center_center, estilo.size40, estilo.borderBtn]}>
                            <Icon
                                type='FontAwesome'
                                style={estilo.tabIcon}
                                name="user"
                            />
                        </TouchableOpacity>
                    </View>

                </View>

                <View style={[estilo.slide, estilo.w100]}>
                    <Swiper autoplay={true}>
                        <ImageBackground
                            style={[estilo.imgBackground, estilo.center_center]}
                            source={require('../../assets/img/compra-casa.jpg')}
                            resizeMode='cover'
                        >
                        </ImageBackground>
                        <ImageBackground
                            style={[estilo.imgBackground, estilo.center_center]}
                            source={require('../../assets/img/compra-carro.jpg')}
                            resizeMode='cover'
                        >
                        </ImageBackground>
                        <ImageBackground
                            style={[estilo.imgBackground, estilo.center_center]}
                            source={require('../../assets/img/compra-moto.jpg')}
                            resizeMode='cover'
                        >
                        </ImageBackground>
                    </Swiper>
                </View>
                <View style={[estilo.msgBar, estilo.w100, estilo.center_around, estilo.row]}>
                    <View style={[estilo.row, estilo.center_around]}>
                        <Icon
                            type='FontAwesome'
                            style={{ fontSize: 24, color: '#fff' }}
                            name="calendar"
                        />
                        <Text style={[estilo.white, { marginLeft: 10, fontSize: 16 }]}>PRÓXIMA ASSEMBLÉIA</Text>
                    </View>
                    <Text style={[estilo.bold, estilo.white, { fontSize: 17 }]}>01/03/2019</Text>
                </View>
                <View style={[estilo.cardContainer, estilo.w100, { padding: 10, alignItems: 'center' }]}>
                    
                    <View style={[
                        estilo.card,
                        estilo.w100,
                        estilo.row,
                        estilo.center_around,
                        estilo.p6, { marginBottom: 3 }]}>
                        <Image
                            style={{ width: 50, height: 50 }}
                            source={require('../../assets/img/lance.png')}
                        />
                        <View style={{ textAlign: 'center' }}>
                            <Text style={[estilo.blue, { fontSize: 16 }]}>OFERTA DE LANCE</Text>
                            <Text style={estilo.bold}>Você já fez o seu lance esse mês?</Text>
                        </View>
                    </View>
                    <Button
                        width={'100%'}
                        height={35}
                        texto='DÊ SEU LANCE AGORA!'
                        corTexto='#fff'
                        corFundo={blue}
                        borderRadius={6}
                    />
                    <View style={[estilo.row, estilo.w100, estilo.start_between, { marginTop: 15 }]}>
                        <TouchableOpacity style={[
                            estilo.card,
                            estilo.column,
                            estilo.p6,
                            estilo.center_center,
                            { width: '48%' }]}
                        >
                            <Image
                                style={{ width: 56, height: 56 }}
                                source={require('../../assets/img/documentos.png')}
                            />
                            <Text style={{ fontSize: 12 }}>Documentos Úteis</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[
                            estilo.card,
                            estilo.column,
                            estilo.p6,
                            estilo.center_center,
                            { width: '48%', textAlign: 'center' }]}
                        >
                            <Image
                                style={{ width: 56, height: 56 }}
                                source={require('../../assets/img/codigo-barra.png')}
                            />
                            <Text style={{ fontSize: 12 }}>Emita aqui 2ª via do boleto</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const estilo = StyleSheet.create({
    container: {
        flex: 11,
        width: '100%',
        backgroundColor: bg_gray
    },
    topBar: {
        flex: 1,
        // height: 70,
        borderBottomColor: second_info,
        borderBottomWidth: 1
    },
    slide: {
        flex: 4.2
        // height: 220
    },
    cardContainer: {
        flex: 4
    },
    msgBar: {
        flex: 0.8,
        height: 50,
        backgroundColor: blue
    },
    
    border: {
        borderColor: 'red',
        borderWidth: 1
    },
    center_center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    start_between: {
        justifyContent: 'space-between',
        alignItems: 'flex-start'
    },
    center_around: {
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    between_center: {
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    w100: { width: '100%' },

    size40: { width: 40, height: 40 },

    borderBtn: { borderRadius: 40, borderColor: blue, borderWidth: 1 },

    column: { flexDirection: 'column' },

    row: { flexDirection: 'row' },

    bold: { fontWeight: 'bold' },

    blueText: { color: blue },

    tabIcon: { fontSize: 24, color: blue },

    imgBackground: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    white: { color: '#fff' },
    blue: { color: blue },

    card: {
        backgroundColor: '#fff',
        borderColor: second_info,
        borderWidth: 1,
        borderRadius: 4
    },
    p6: { padding: 6 }

})
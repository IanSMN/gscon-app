import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
import { blue } from '../../utils/Colors';

const BotoesTipo = (props) => (
    <TouchableOpacity
        onPress={props.onPress}
        style={[
            estilo.iconButton,
            props.tipoSelecionado === props.valor ?
                estilo.btnSelected :
                estilo.notSelected
        ]}
    >
        <Icon
            type='FontAwesome'
            name={props.icone}
            style={[
                props.tipoSelecionado === props.valor ?
                    { color: '#fff' } : { color: blue }, { fontSize: 48 }
            ]}
        />
    </TouchableOpacity >
)

export default BotoesTipo

const estilo = StyleSheet.create({
    iconButton: {
        width: 85,
        height: 56,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 2,
        borderRadius: 4
    },
    notSelected: {
        backgroundColor: '#fff',
        borderColor: blue,
        borderWidth: 1,
    },
    btnSelected: {
        backgroundColor: blue
    }
})
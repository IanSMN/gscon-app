import React from 'react'
import { Text, View, StyleSheet, Image, Modal, TouchableOpacity, ScrollView } from 'react-native'
import { TextInputMask } from 'react-native-masked-text';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { blue, clear, bg_gray, dark_gray, second_info } from '../../utils/Colors';
import Button from '../../components/Button/Button';
import BotoesTipo from './BotoesTipo';
import * as simulacaoActions from '../../actions/simulacaoActions';
import InputDropdown from '../../components/Dropdown/inputDropdown';
import { Container } from 'native-base';

class Simulacao extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            tipoPlano: 'casa',
            tipoSimulacao: 'bem',
            valorParcela: 0,
            valorBem: 0,
            prazo: 0,
            prazoSelecionado: null,
            mostrarDrop: false,
            listaDrop: [
                { label: "05 meses", value: "05" },
                { label: "10 meses", value: "10" },
                { label: "15 meses", value: "15" },
                { label: "20 meses", value: "20" },
                { label: "25 meses", value: "25" },
            ]
        }
    }

    _handlePlano = (plano) => {
        this.setState({
            tipoPlano: plano,
            tipoSimulacao: null,
            valorParcela: 0,
            prazo: 0
        })
    }

    _handleSimulacao = (simulacao) => {
        this.setState({
            tipoSimulacao: simulacao,
            valorParcela: 0,
            prazo: 0
        })
    }

    onValueChange(value) {
        this.setState({ prazoSelecionado: value })
    }

    _handleDropdown(value) {
        this.setState({ mostrarDrop: value })
    }

    _handleOpcao(opcao) {
        this.setState({
            prazoSelecionado: opcao.label,
            mostrarDrop: false
        })
    }


    render() {
        return (
            <ScrollView
                style={{ minHeight: '100%' }}
                scrollEnabled={true}
                contentContainerStyle={estilo.container}
            >
                {/* <View style={estilo.fakeToolbar}>
                    <Image
                        style={{ height: 40 }}
                        source={require('../../assets/img/groscon-logo.png')}
                        resizeMode={'contain'}
                    />
                </View> */}
                <View style={estilo.titleContainer}>
                    <Text style={estilo.blueTitle}>Consórcio</Text>
                    <Text style={estilo.blueTitle}>Simulador</Text>
                </View>

                <View style={estilo.titleContainer}>
                    <Text style={estilo.info}>Selecione a categoria do plano:</Text>
                </View>

                <View style={[estilo.rowContainer, estilo.jSA]}>

                    <BotoesTipo
                        onPress={() => this._handlePlano('casa')}
                        valor={'casa'}
                        tipoSelecionado={this.state.tipoPlano}
                        icone='home'
                    />
                    <BotoesTipo
                        onPress={() => this._handlePlano('carro')}
                        valor={'carro'}
                        tipoSelecionado={this.state.tipoPlano}
                        icone='car'
                    />
                    <BotoesTipo
                        onPress={() => this._handlePlano('caminhao')}
                        valor={'caminhao'}
                        tipoSelecionado={this.state.tipoPlano}
                        icone='truck'
                    />

                </View>
                {this.state.tipoPlano !== null ?
                    <React.Fragment>

                        <View style={estilo.titleContainer}>
                            <Text style={estilo.info}>
                                Como quer fazer a simulação?
                            </Text>
                        </View>

                        <View style={[estilo.rowContainer, estilo.jSA]}>

                            <View style={estilo.buttonView}>
                                <Button
                                    borda={this.state.tipoSimulacao === 'parcela' ? false : true}
                                    corBorda={blue}
                                    corFundo={this.state.tipoSimulacao === 'parcela' ? blue : 'transparent'}
                                    texto='VALOR DA PARCELA'
                                    corTexto={this.state.tipoSimulacao === 'parcela' ? '#fff' : blue}
                                    width={'100%'}
                                    onPress={() => this._handleSimulacao('parcela')}
                                />
                            </View>

                            <View style={estilo.buttonView}>
                                <Button
                                    borda={this.state.tipoSimulacao === 'bem' ? false : true}
                                    corBorda={blue}
                                    corFundo={this.state.tipoSimulacao === 'bem' ? blue : 'transparent'}
                                    texto='VALOR DO BEM'
                                    corTexto={this.state.tipoSimulacao === 'bem' ? '#fff' : blue}
                                    width={'100%'}
                                    onPress={() => this._handleSimulacao('bem')}
                                />
                            </View>
                        </View>
                    </React.Fragment>
                    : null
                }
                {this.state.tipoSimulacao !== null ?
                    <React.Fragment>
                        <View style={estilo.titleContainer}>
                            {this.state.tipoSimulacao === 'bem' ?
                                <Text style={estilo.info}>
                                    Informe o valor do Bem:
                            </Text>
                                :
                                <Text style={estilo.info}>
                                    Informe o valor da Parcela
                                </Text>
                            }
                        </View>

                        <TextInputMask
                            type={'money'}
                            options={{
                                precision: 2,
                                separator: ',',
                                delimiter: '.',
                                unit: 'R$',
                                suffixUnit: ''
                            }}
                            value={this.state.tipoSimulacao === 'parcela' ?
                                this.state.valorParcela : this.state.valorBem}
                            onChangeText={text => {
                                this.state.tipoSimulacao === 'parcela' ?
                                    this.setState({ valorParcela: text })
                                    :
                                    this.setState({ valorBem: text })
                            }
                            }
                            style={[estilo.input, estilo.w318, { paddingLeft: 12 }]}
                        />
                    </React.Fragment>
                    : null
                }

                {this.state.valorParcela !== 0 || this.state.valorBem !== 0 ?
                    <React.Fragment>
                        <View style={estilo.titleContainer}>
                            <Text style={estilo.info}>
                                Qual prazo você deseja?
                            </Text>
                        </View>

                        <View style={{ width: 318 }}>
                            <InputDropdown
                                placeholder={this.state.prazoSelecionado !== null ?
                                    this.state.prazoSelecionado : "Selecione o prazo:"}
                                clique={() => this.setState({ mostrarDrop: true })}
                            />
                        </View>
                    </React.Fragment>
                    : null
                }

                <View style={[estilo.rowContainer, estilo.jFE, { marginTop: 15 }]}>
                    {
                        this.state.prazoSelecionado ?
                            <Button
                                width={180}
                                texto='VER RESULTADOS'
                                corFundo={blue}
                                corTexto={clear}
                                onPress={() => simulacaoActions.handle_simulacao(
                                    this.props.navigation,
                                    this.state.tipoPlano,
                                    this.state.valorParcela,
                                    this.state.valorBem,
                                    this.state.prazo
                                )}
                                rightIcon={'chevron-right'}
                            />
                            : null
                    }
                </View>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.mostrarDrop ? true : false}
                    onRequestClose={() => this.setState({ mostrarDrop: false })}
                >
                    <View style={estilo.wrapper}>
                        <View style={estilo.content}>
                            <View style={estilo.header}>
                                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>
                                    Selecione o prazo:
                                </Text>
                            </View>
                            {this.state.listaDrop.map(opcao => (
                                <TouchableOpacity
                                    style={estilo.opcaoContent}
                                    key={opcao.value}
                                    onPress={() => this._handleOpcao(opcao)}
                                >
                                    <Text style={{ fontSize: 20 }}>
                                        {opcao.label}
                                    </Text>
                                </TouchableOpacity>
                            ))}
                        </View>
                    </View>
                </Modal>

            </ScrollView >
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    actions: {
        simulacaoActions: bindActionCreators(simulacaoActions, dispatch)
    }
})

export default connect(null, mapDispatchToProps)(Simulacao)

const estilo = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: bg_gray,
        // paddingTop: 60,
        // overFlow: 'scroll'
    },
    fakeToolbar: {
        position: 'absolute',
        width: '100%',
        height: 60,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 4,
        top: 0,
        left: 0,
    },
    blueTitle: {
        color: blue,
        fontWeight: 'bold',
        fontSize: 36
    },
    titleContainer: {
        marginTop: 8,
        marginBottom: 8
    },
    info: {
        color: dark_gray,
        fontSize: 18
    },
    rowContainer: {
        width: 318,
        flexDirection: 'row',
        alignItems: 'center'
    },
    jSA: { justifyContent: 'space-around' },
    jCenter: { justifyContent: 'center' },
    jFE: { justifyContent: 'flex-end' },
    buttonView: {
        width: 155
    },
    input: {
        height: 50,
        fontSize: 20,
        backgroundColor: '#fff',
        color: dark_gray,
        borderColor: dark_gray,
        borderWidth: 1,
        borderRadius: 4
    },
    w318: { width: 318 },
    w100: { width: 80, textAlign: 'center' },
    wrapper: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        top: 0,
        left: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 9
    },
    content: {
        width: '98%',
        backgroundColor: '#fff',
        padding: 10,
        borderRadius: 4
    },
    opcaoContent: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        borderBottomWidth: 1,
        borderBottomColor: second_info
    },
    header: {
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: second_info,
        height: 50,
        paddingRight: 10,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#fff'
    }
})
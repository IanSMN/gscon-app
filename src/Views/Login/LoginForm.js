import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { dark_gray, blue, clear } from '../../utils/Colors';
import Input from '../../components/Input/Input';
import Button from '../../components/Button/Button';
import FlatButton from '../../components/FlatButton/FlatButton';

const LoginForm = (props) => (
    <Formik
        initialValues={{ email: '', senha: '' }}
        onSubmit={props.onSubmit}
        validateOnChange={true}
        validateOnBlur={true}
        validationSchema={
            Yup.object().shape({
                email: Yup.string()
                    .email('Digite um formato válido de email')
                    .required('Campo obrigatório'),
                senha: Yup.string()
                    .required('Campo obrigatório')
            })
        }
        render={({
            values,
            handleSubmit,
            setFieldValue,
            errors,
            touched,
            setFieldTouched,
            isValid
        }) => (
                <View style={{
                    width: '100%',
                    flexDirection: 'column',
                    height: 350,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>

                    <View style={estilo.inputView}>

                        <View style={{ width: '100%' }}>
                            <Input
                                // autoFocus={true}
                                label="Email"
                                placeholder="Ex mariaRibeiro@gmail.com"
                                value={values.email}
                                onChange={setFieldValue}
                                onTouch={setFieldTouched}
                                name="email"
                                error={touched.email && errors.email}
                                keyboardType={'email-address'}
                                returnKeyType='next'
                                onSubmitEditing={() => this.senha.focus()}
                                required={true}
                                autoFocus={true}
                            />
                        </View>

                        <View style={{ width: '100%', marginTop: 20 }}>
                            <Input
                                refInput={(senha) => this.senha = senha}
                                label="Senha"
                                value={values.senha}
                                onChange={setFieldValue}
                                onTouch={setFieldTouched}
                                name="senha"
                                error={touched.senha && errors.senha}
                                keyboardType={'default'}
                                returnKeyType='done'
                                type='password'
                                required={true}
                            />
                        </View>

                    </View>

                    <View style={estilo.rowContent}>
                        <Button
                            width='48%'
                            corFundo={blue}
                            corTexto={clear}
                            texto='ENTRAR'
                            onPress={handleSubmit}
                            disabled={!isValid}
                        />
                        <Button
                            width='48%'
                            corFundo={dark_gray}
                            corTexto={clear}
                            texto='1º ACESSO'
                            onPress={props.acaoSecundaria}
                        />
                    </View>

                    <View>
                        <FlatButton
                            texto='Esqueceu a senha? Clique aqui'
                            corTexto={blue}
                            width='100%'
                            onPress={props.esqueciSenha}
                        />
                    </View>

                </View>
            )}
    />
)

const estilo = StyleSheet.create({
    inputView: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        flex: 1,
        position: 'relative'
    },
    rowContent: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    w90: {
        width: '90%',
        marginTop: 25,
        justifyContent: 'center'
    }
})

export default LoginForm
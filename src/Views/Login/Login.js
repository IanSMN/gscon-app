import React from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { bg_gray } from '../../utils/Colors';
import LoginForm from './LoginForm';

export default LoginPage = (props) => {
    return (
        <View style={estilo.container}>
            <View style={estilo.wrapper}>
                <Image
                    source={require('../../assets/img/groscon-logo.png')}
                    resizeMode={'contain'}
                    style={estilo.logo}
                />
                <LoginForm
                    onSubmit={() => props.navigation.navigate('Home', { usuario: true })}
                    acaoSecundaria={() => console.warn('VOU PARA O CADASTRO')}
                    esqueciSenha={() => console.warn('ESQUECI A SENHA')}
                />
            </View>
        </View>
    )
}

const estilo = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: bg_gray
    },
    wrapper: {
        width: '80%',
        minWidth: 320,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
    },
    logo: {
        width: '100%'
    }
})
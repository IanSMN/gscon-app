import React from 'react'
import { Text } from 'react-native'
import StackNoUser from '../../nav/StackNoUser';
import StackUser from '../../nav/StackUser';

export default class Home extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        if (this.props.navigation.state.params.usuario === true) {
            return (
                <StackUser />
            )
        } else {
            return <StackNoUser />
        }
    }
}
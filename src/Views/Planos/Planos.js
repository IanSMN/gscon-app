import React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import { Container, Content } from 'native-base';
import Toolbar from '../../components/Toolbar/toolbar';
import {
    bg_gray,
    blue,
    dark_gray,
    second_info,
    green,
    clear
} from '../../utils/Colors';
import Button from '../../components/Button/Button';

const Planos = (props) => (
    <Container>
        <Toolbar
            leftIcon="arrow-left"
            leftAction={() => props.navigation.navigate('Simulacao')}
            titulo="Planos"
        />
        <Content contentContainerStyle={estilo.container}>
            <View style={estilo.titleContainer}>
                {/* <Text style={estilo.blueTitle}>Planos</Text> */}
                <Text style={[estilo.info, { fontWeight: 'bold' }]}>Com base no seu perfil,
                encontramos os seguintes planos:</Text>
            </View>

            <View style={estilo.rowContainer}>
                <View style={estilo.card}>
                    <Image
                        source={require('../../assets/img/logo-menor.png')}
                        style={{ width: '80%' }}
                        resizeMode={'contain'}
                    />
                    <View style={estilo.info_wrapper}>
                        <Text style={estilo.blueText}>
                            R$ 34.210,00
                    </Text>
                        <Text style={estilo.desc}>
                            20 parcelas
                    </Text>
                    </View>

                    <View style={estilo.info_wrapper}>
                        <Text style={estilo.blueText}>
                            R$1.8881,55
                    </Text>
                        <Text style={estilo.desc}>por mês</Text>
                    </View>

                    <View style={estilo.info_wrapper}>
                        <Text style={estilo.blueText}>
                            R$ 37.631,00
                        </Text>
                        <Text style={estilo.desc}>ao final do período</Text>
                    </View>
                </View>

                <View style={estilo.card}>
                    <Text style={estilo.desc}>
                        Financiamento
                    </Text>
                    <Text style={estilo.desc}>
                        Convencional
                    </Text>
                    <View style={estilo.info_wrapper}>
                        <Text style={estilo.blueText}>
                            R$ 34.210,00
                    </Text>
                        <Text style={estilo.desc}>
                            20 parcelas
                    </Text>
                    </View>

                    <View style={estilo.info_wrapper}>
                        <Text style={estilo.blueText}>
                            R$2.062,00
                    </Text>
                        <Text style={estilo.desc}>por mês</Text>
                    </View>
                    <View style={estilo.info_wrapper}>
                        <Text style={estilo.blueText}>
                            R$ 41.240,20
                    </Text>
                        <Text style={estilo.desc}>ao final do período</Text>
                    </View>
                </View>
            </View>
            <View style={estilo.info_wrapper}>
                <Text style={[estilo.info, { fontSize: 24, fontWeight: 'bold' }]}>
                    Com o Consórcio Groscon você</Text>
                <Text style={[estilo.infomoney, { fontSize: 24 }]}>economizará:</Text>
                <Text style={[estilo.infomoney, { fontSize: 28 }]}>R$3.609,18</Text>
            </View>

            <View style={{
                width: '100%',
                // marginTop: 15,
                justifyContent: 'flex-end',
                alignItems: 'flex-end',
                flex: 1
            }}>
                <Button
                    texto="CONTRATAR"
                    corFundo={blue}
                    corTexto={clear}
                    rightIcon={'chevron-right'}
                    width={160}
                />
            </View>

        </Content>
    </Container >
)

const estilo = StyleSheet.create({
    container: {
        flex: 9,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: bg_gray,
        padding: 10
    },
    blueTitle: {
        color: blue,
        fontWeight: 'bold',
        fontSize: 36
    },
    titleContainer: {
        // marginTop: 10,
        // marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
        width: 315,
        flex: 2
    },
    info: {
        color: dark_gray,
        fontSize: 18,
        textAlign: 'center'
    },
    rowContainer: {
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        width: '100%',
        flexDirection: 'row',
        // marginBottom: 10,
        flex: 4
    },
    card: {
        backgroundColor: '#fff',
        borderRadius: 4,
        borderColor: second_info,
        borderWidth: 1,
        width: '48%',
        minWidth: 145,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 4,
        paddingBottom: 4
    },
    blueText: {
        fontWeight: 'bold',
        color: blue,
        fontSize: 20
    },
    desc: {
        color: dark_gray,
        fontSize: 17
    },
    info_wrapper: {
        // marginTop: 5,
        // marginBottom: 5,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 2
    },
    infomoney: { color: green, fontWeight: 'bold' }
})

export default Planos